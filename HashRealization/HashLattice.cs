﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grid
{
    class HashGenericLattice<T> : ILattice<T>, IComparison<T>
    {
        private Dictionary<T, HashSet<T>> nodes
        {
            get { return nodes; }
            set {nodes = value;}
        }
        private List<List<T>> accum;

        public HashGenericLattice(IComparison<T> comparison)
        {

        }

        public Boolean Equals(T el1)
        {

            return false;
        }


        public bool IsLessOrEqual(T el1, T el2)
        {
            Type t1 = el1.GetType();
            Type t2 = el2.GetType();
            if (t1.IsSubclassOf(t2))
                return true;
            return false;
        }

        public HashGenericLattice()
        {
            nodes = new Dictionary<T, HashSet<T>>();
        }

        public void AddItem(T item)
        {
            if (nodes.ContainsKey(item))
            {
                foreach(var el in nodes.Keys)
                    if (IsLessOrEqual(item, el))
                        nodes[el].Add(item);
            }
            else
            {
                nodes.Add(item, new HashSet<T>() { });
                foreach(var el in nodes.Keys)
                    if (IsLessOrEqual(item, el))
                        nodes[el].Add(item);
            }
        }

        public bool IsExtended(T parent, T child)
        {
            if (nodes.ContainsKey(parent))
                if (nodes[parent].Contains(child))
                    return true;
                else
                    return nodes[parent].Any(c => IsExtended(c, child));

            return false;
        }

        public List<List<T>> GetHierarchy(T parent, T child)
        {
            accum = new List<List<T>>();
            accum.Add(new List<T>());
            GetHierarchy(parent, child, accum[accum.Count - 1]);
            return accum;
        }

        public List<T> GetHierarchy(T parent, T child, List<T> acc)
        {
            if (nodes.ContainsKey(parent))
            {
                acc.Add(parent);
                if (nodes[parent].Contains(child))
                {
                    acc.Add(child);
                    return acc;
                }
                else
                {
                    foreach (var c in nodes[parent])
                    {
                        //if (c == parent)
                        {
                            accum.Add(new List<T>());
                            acc = accum[accum.Count - 1];
                        }
                        GetHierarchy(c, child, acc);
                    }
                }
            }
            return acc;
        }

        public ILattice<T> Intersect(ILattice<T> first, ILattice<T> second)         //Find min Lattice
        {
            if (first is HashGenericLattice<T> && second is HashGenericLattice<T>)
            {
                HashGenericLattice<T> firstLat = (HashGenericLattice<T>)first;
                HashGenericLattice<T> secLat = (HashGenericLattice<T>)second;
                var result = new HashGenericLattice<T>();

                foreach (var key in firstLat.nodes.Keys)
                {
                    if (secLat.nodes.ContainsKey(key))
                    {
                        var hSet = new HashSet<T>();
                        foreach (var el in firstLat.nodes[key])
                        {
                            if (secLat.nodes[key].Contains(el))
                                hSet.Add(el);
                        }

                        result.nodes.Add(key, hSet);
                    }
                }
                
                
                return result;
            }

            return null;
        }
        public ILattice<T> Unify(ILattice<T> first, ILattice<T> second)
        {
            if (first is HashGenericLattice<T> && second is HashGenericLattice<T>)
            {
                HashGenericLattice<T> firstLat = (HashGenericLattice<T>)first;
                HashGenericLattice<T> secLat = (HashGenericLattice<T>)second;
                var result = firstLat;

                foreach (var key in secLat.nodes.Keys)
                {
                    if (!result.nodes.ContainsKey(key))
                    {
                        result.nodes.Add(key, secLat.nodes[key]);
                    }
                    else
                    {
                        foreach (var el in secLat.nodes[key])
                        {
                            if (!result.nodes[key].Contains(el))
                                result.nodes[key].Add(el);
                        }
                    }
                }
                return result;
            }
            
            return null;
        }
        public List<T> Infinum(ILattice<T> lattice)                 //Not Done. Just min. But it's ok)
        {
            if (lattice is HashGenericLattice<T>)
            {
                var result = new List<T>();
                HashGenericLattice<T> lat = (HashGenericLattice<T>)lattice;
                foreach (var key in lat.nodes.Keys)
                {
                    if (lat.nodes[key].Count == 0)
                    {
                        result.Add(key);
                    }
                }
                return result;
            }
            
            return null;
        }
        public List<T> Supremum(ILattice<T> lattice)
        {
            if (lattice is HashGenericLattice<T>)
            {
                HashGenericLattice<T> lat = (HashGenericLattice<T>)lattice;

            }

            return null;
        }
        public List<T> Max(ILattice<T> lattice)
        {
            if (lattice is HashGenericLattice<T>)
            {
                var result = new List<T>();
                var exist = false;
                HashGenericLattice<T> lat = (HashGenericLattice<T>)lattice;
                foreach (var key in lat.nodes.Keys)
                {
                    foreach (var value in lat.nodes.Values)
                    {
                        if (value.Contains(key))
                        {
                            exist = false;
                        }
                        else
                        {
                            exist = true;
                        }
                    }
                    if (exist)
                        result.Add(key);
                }
                return result;
            }

            return null;
        }
        public List<T> Min(ILattice<T> lattice)
        {
            if (lattice is HashGenericLattice<T>)
            {
                var result = new List<T>();
                HashGenericLattice<T> lat = (HashGenericLattice<T>)lattice;
                foreach (var key in lat.nodes.Keys)
                {
                    if (lat.nodes[key].Count == 0)
                    {
                        result.Add(key);
                    }
                }
                return result;
            }

            return null;
        }


    }
}
