﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grid
{
    class IntHashLattice
    {
        // private Dictionary<int, string> associate;
        //private HashSet<String> children; 
        private Dictionary<int, HashSet<int>> nodes;
        private List<List<int>> accum;

        public IntHashLattice()
        {
            nodes = new Dictionary<int, HashSet<int>>();
        }

        public void AddItem(int parent, int child)
        {
            if (nodes.ContainsKey(parent))
            {
                if (!nodes[parent].Contains(child))
                    nodes[parent].Add(child);
            }
            else
                nodes.Add(parent, new HashSet<int>() { child });
        }

        public bool DeleteItem(int item)
        {
            if (nodes.ContainsKey(item))
                if (nodes[item].Contains(item))
                    return true;
                else
                    return nodes[item].Any(c => DeleteItem(c));
            return false;
        }

        public bool IsExtended(int parent, int child)
        {
            if (nodes.ContainsKey(parent))
                if (nodes[parent].Contains(child))
                    return true;
                else
                    return nodes[parent].Any(c => IsExtended(c, child));

            return false;
        }

        public List<List<int>> GetHierarchy(int parent, int child)
        {
            accum = new List<List<int>>();
            accum.Add(new List<int>());
            GetHierarchy(parent, child, accum[accum.Count-1]);
            return accum;
        }

        override
        public String ToString()
        {
            String result = "";
            foreach (var item in accum)
            {
                foreach (var i in item)
                {
                    result += i + ",";
                }
                result += "nyak";
            }
            return result;
        }


        public List<int> GetHierarchy(int parent, int child, List<int> acc)
        {
            if (nodes.ContainsKey(parent))
            {
                acc.Add(parent);
                if (nodes[parent].Contains(child)){
                    acc.Add(child);
                    return acc;
                }  
                else
                {
                    foreach (var c in nodes[parent])
                    {
                        if (c == parent)
                        {
                            accum.Add(new List<int>());
                            acc = accum[accum.Count-1];
                        }
                        GetHierarchy(c, child, acc);
                    }
                }
            }
            return acc;
        }
    }
}
