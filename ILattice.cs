﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Grid
{
    public interface ILattice<T>
    {
        ILattice<T> Intersect(ILattice<T> first, ILattice<T> second);
        ILattice<T> Unify(ILattice<T> first, ILattice<T> second);
        List<T> Infinum(ILattice<T> lattice);
        List<T> Supremum(ILattice<T> lattice);
        List<T> Max(ILattice<T> lattice);
        List<T> Min(ILattice<T> lattice);
    }
}
