﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grid
{
    //[<EntryPoint>]
    class Tests
    {
        public class TypeComparison : IComparison<Type>
        {
            public static TypeComparison Comparison { get; private set; }

            static TypeComparison()
            {
                Comparison = new TypeComparison();
            }

            ///
        }

        static void Main(string[] args) //nUnit 
        {
            var lattice = new HashGenericLattice<Type>(TypeComparison.Comparison);

            IntHashLattice tl = new IntHashLattice();
            tl.AddItem(1, 2);
            tl.AddItem(1, 3);
            tl.AddItem(1, 4);
            tl.AddItem(2, 5);
            tl.AddItem(2, 6);
            tl.AddItem(2, 7);
            tl.AddItem(3, 8);
            tl.AddItem(3, 7);
            tl.AddItem(4, 9);
            Console.WriteLine(tl.IsExtended(1,9)); //true
            Console.WriteLine(tl.IsExtended(3,9)); //false
            Console.WriteLine(tl.IsExtended(1,2)); //true
            Console.WriteLine((tl.GetHierarchy(1, 2)).ToString()); //1,2
            Console.ReadKey();
        }
    }
}
