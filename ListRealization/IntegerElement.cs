﻿using System;

namespace Mephi.Cybernetics.Csit.Lattice
{
    public class IntegerElement : ListLatticeElement<int>
    {
        private int el;

        public IntegerElement(int value) : base(value)
        {
        }

        public override Boolean Equals(int el1)
        {
            return el == el1;
        }

        public override Boolean IsLessOrEqual(int el1, int el2)
        {
            return el <= el1;
        }
    }
}
