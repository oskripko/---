﻿using System;
using System.Collections.Generic;
using Grid;

namespace Mephi.Cybernetics.Csit.Lattice
{
    public class ListLatticeElement<T> : IComparison<T>
    {
        private List<ListLatticeElement<T>> next, previous;

        private T _value;
        public T value {
            get { return _value; }
        }

        public ListLatticeElement() { }

        public ListLatticeElement(T value) 
        {
            this._value = value;
            this.next = new List<ListLatticeElement<T>>();
            this.previous = new List<ListLatticeElement<T>>();
        }

        public virtual Boolean Equals(T el) 
        {
            throw new NotSupportedException();
        }

        public virtual Boolean IsLessOrEqual(T el1, T el2)
        {
            throw new NotSupportedException();
        }

        public List<ListLatticeElement<T>> getNextLinks()
        {
            return next;
        }

        public List<ListLatticeElement<T>> getPreviousLinks()
        {
            return previous;
        }
    }
}
