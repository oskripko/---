﻿using System;
using System.Linq;
using Grid;
using System.Collections.Generic;

namespace Mephi.Cybernetics.Csit.Lattice
{
    public class ListLattice<T> : ILattice<T>
    {

        private ListLatticeElement<T> dummy_root;

        public ListLattice()
        {
            dummy_root = new ListLatticeElement<T>();
        }

        public Boolean Add(ListLatticeElement<T> obj)
        {   
            return Add(dummy_root, obj);
        }

        private Boolean Add(ListLatticeElement<T> currentRoot, ListLatticeElement<T> obj)
        {
            var nextLinks = currentRoot.getNextLinks();
            var prevLinks = currentRoot.getPreviousLinks();
           
            if (!currentRoot.IsLessOrEqual(currentRoot.value, currentRoot.value))       //Check 2 value
                foreach (var element in nextLinks)
                    return Add(element, obj);
            else if (!nextLinks.Any())
            {
                nextLinks.Add(obj);
                return true;
            }
            else if (!prevLinks.Any())
            {
                ListLatticeElement<T> temp = new ListLatticeElement<T>();
                temp.getNextLinks().Add(currentRoot);
                currentRoot = obj;
                return true;
            }
            else
            {
                ListLatticeElement<T> temp = new ListLatticeElement<T>();
                temp = obj;
                foreach (var element in nextLinks)
                    currentRoot.getNextLinks().Remove(element);
                currentRoot.getNextLinks().Add(temp);
                foreach (var element in nextLinks)
                    temp.getNextLinks().Add(element);
            }
            return true;
        }

        public Boolean Remove(ListLatticeElement<T> obj) 
        {

            var nextLinks = obj.getNextLinks();
            var prevLinks = obj.getPreviousLinks();


            ListLatticeElement<T> temp = new ListLatticeElement<T>();
            ListLatticeElement<T> isNeededEl = new ListLatticeElement<T>();
            var results = nextLinks.FindAll(null);//findSuchElementsThat
            foreach (var pElement in prevLinks)
            {
                foreach (var nElement in nextLinks)
                    pElement.getNextLinks().Add(nElement);
            }
            temp.getNextLinks().RemoveAll(element => elIsObj(element, obj));

            return true;
        }

        public Boolean Contains(ListLatticeElement<T> obj) 
        {
            //То же, что и Find. Если нашли, то true. При Exception, False.
            return true;
        }


        private static bool elIsObj(ListLatticeElement<T> testElement, ListLatticeElement<T> obj)
        {
            if (testElement == obj)
                return true;
            else
                return false;
        }

        public ILattice<T> Intersect(ILattice<T> first, ILattice<T> second)
        {
            if (first is ListLatticeElement<T> && second is ListLatticeElement<T>)
            {
                ListLatticeElement<T> firstLat = (ListLatticeElement<T>)first;
                ListLatticeElement<T> secLat = (ListLatticeElement<T>)second;

                var fNextLinks = firstLat.getNextLinks();

                var sNextLinks = secLat.getNextLinks();

                var result = new ListLattice<T>();

                foreach (var el in fNextLinks)
                {
                    if (sNextLinks.Contains(el))
                        result.Add(el);
                }

                return result;
            }

            return null;
        }

        public ILattice<T> Unify(ILattice<T> first, ILattice<T> second)
        {
            if (first is ListLatticeElement<T> && second is ListLatticeElement<T>)
            {
                ListLatticeElement<T> firstLat = (ListLatticeElement<T>)first;
                ListLatticeElement<T> secLat = (ListLatticeElement<T>)second;

                var fNextLinks = firstLat.getNextLinks();

                var sNextLinks = secLat.getNextLinks();

                var result = new ListLattice<T>();

                foreach (var el in fNextLinks)
                {
                    result.Add(el);
                }
                foreach (var el in fNextLinks)
                {
                    if (!sNextLinks.Contains(el))
                        result.Add(el);
                }

                return result;
            }

            return null;
        }

        public List<ListLatticeElement<T>> Infinum(ILattice<T> lattice)
        {
            if (lattice is ListLatticeElement<T>)
            {
                ListLatticeElement<T> lat = (ListLatticeElement<T>)lattice;

                var nextLinks = lat.getNextLinks();

                var result = new List<ListLatticeElement<T>>();

                while (nextLinks != null)
                {
                    foreach (var el in nextLinks)
                    if (el.getNextLinks() == null)
                    {
                            result.Add(el);
                    }
                }

                return result;
            }

            return null;
        }

        public List<T> Supremum(ILattice<T> lattice)
        {
            throw new NotImplementedException();
        }

        public List<T> Max(ILattice<T> lattice)
        {
            throw new NotImplementedException();
        }

        public List<T> Min(ILattice<T> lattice)
        {
            throw new NotImplementedException();
        }
    }
}
