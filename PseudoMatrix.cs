﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.T

namespace Grid {


    public class Grid<T> {
        private List<String> Items;
        private bool[,] ComparationsMatrix;
        //private bool[,] IsComparableMatrix;

        public void AddItem(String item, List<String> extends) {
            Items.Add(item);
            var NewComparationsMatrix = new bool[Items.Count, Items.Count];


            for (int i = 0; i < Items.Count; i++)
                for (int j = i; j < Items.Count; j++)
                    NewComparationsMatrix[i, j] = ComparationsMatrix[i, j];

            for (int i = 0; i < Items.Count; i++)
                if (extends.Contains(Items[i])) {
                    NewComparationsMatrix[i, Items.Count] = true;
                    NewComparationsMatrix[Items.Count, i] = true;
                }

        }

        public int Find(String item) {
            for (int i = 0; i < Items.Count; i++)
                if (Items[i] == item)
                    return i;
            return -1;
        }

        public int GetItem(String item) {
            var i = Find(item);
            if (!(i == -1)) {
                Items.Add(item);
                return Items.Count - 1;
            }
            return i;
            }


        public void AddExtendingArc(String Parent, String Child) {
            var childId = GetItem(Child);
            var parentId = GetItem(Parent);
            ComparationsMatrix[childId, parentId] = true;
        }



      //  public bool IsExtended(String aClass, String bClass) {
       //     var aId = GetItem(aClass);
      //      var bId = GetItem(bClass);
       //     foreach (var k in Items)
                

      //      return false;
      //  }

    //    public void f()

       // public void RemoveTrans() {
       //     ComparationsMatrix = new int[Items.Count, Items.Count];
       //     for (int i = 0; i < Items.Count; i++)
        //        for (int j = i; j < Items.Count; j++)
        //            if (IsComparableMatrix[i, j])
        //                ComparationsMatrix[i, j] = Items[i].CompareTo(Items[j]);
       // }
    }
}
